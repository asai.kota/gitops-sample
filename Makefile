CONTAINER_IMAGE=registry.gitlab.com/asai.kota/gitops-sample
VERSION?=$(shell git describe --tags --match="v*" --abbrev=14 HEAD)

DOCKER_BUILD_OPTS=""
ifeq ($(shell uname),Darwin)
	DOCKER_BUILD_OPTS=--platform linux/amd64
endif

.PHONY: build
build:
	go build -o server main.go

.PHONY: image-build
image-build:
	docker build $(DOCKER_BUILD_OPTS) -t $(CONTAINER_IMAGE):$(VERSION) .
	docker tag $(CONTAINER_IMAGE):$(VERSION) $(CONTAINER_IMAGE):latest

.PHONY: image-push
image-push:
	docker push $(CONTAINER_IMAGE):$(VERSION)
	docker push $(CONTAINER_IMAGE):latest
